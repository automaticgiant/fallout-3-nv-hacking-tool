import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;

/**
 * Tool to assist with Fallout 3/NV terminal hacking. Input possible passwords 
 * and guesses, and program will help you pick likely passwords.
 * @author Hunter Morgan
 *
 */
class hacker {

	/**
	 * HashSet of the working set of possible passwords
	 */
	static ArrayList<String> possibles = new ArrayList<>();
//	/**
//	 * Subset of possibles we will menu around with
//	 */
//	static char[][] menuArray;
	/**
	 * stdin Scanner
	 */
	static Scanner stdin = new Scanner(System.in);
	/**
	 * String slot for current input
	 */
	static String input;
	/**
	 * current password length
	 */
	static int workingLength;
	
	/**
	 * main routine
	 * @param args moot/formality
	 */
	public static void main(String[] args) {
		workingLength = 0;
		while (true) {
			while (possibles.size() == 0) getPossibles();
			if ( possibles.size() != 0 ) menu();
		}
	}
	
	/**
	 * this routine generates and processes an action menu
	 */
	@SuppressWarnings("boxing")
	public static void menu() {
//		menuArray = (char[][]) possibles.toArray();
		int columns = 80 / (workingLength + 3);
		int item = 0;
//		for ( char[] possible : menuArray ) {
		for ( String possible : possibles ) {
			item++;
			System.out.print(item + ":" + possible + " ");
			if ( item % columns == 0 ) System.out.println();
			}
		System.out.println();
		System.out.println("add  remove  guess  restart  exit");
		input = stdin.next();
		while ( input.equals("add") || input.equals("remove") ||
				input.equals("guess") || input.equals("restart") ||
				input.equals("exit") ) {
			switch (input) {
				case "add":
					getPossibles();
					break;
				case "remove":
					System.out.print("Which one: ");
					input = stdin.next();
					possibles.remove(Integer.parseInt(input)-1);
					break;
				case "guess":
					System.out.print("Which one: ");
					input = stdin.next();
					while (!(0<Integer.parseInt(input) &&
						Integer.parseInt(input)<=possibles.size())) {
						System.out.print("Bad input. Try again: ");
						input = stdin.next();
					}
					String guess = possibles.get(Integer.valueOf(input)-1);
					possibles.remove(Integer.valueOf(input)-1);
					System.out.print("How many correct? ");
					input = stdin.next();
					for ( int i = 0 ; i < possibles.size() ; i++ ) {
						int matches = 0;
						for ( int letter = 0 ; letter < workingLength;
								letter++ ) if ( guess.charAt(letter) ==
										possibles.get(i).charAt(letter) ) matches++; 
						if ( matches < Integer.valueOf(input) ) {
							possibles.remove(i);
							i--;
						}
					}
					break;
				case "restart":
					workingLength = 0;
					possibles=new ArrayList<>();
				break;
			case "exit":
					System.exit(0);
				break; // yeah right!
			default:
				input = stdin.next();
				break;
			}
		}
	}

//	/**
//	 * this routine analyzes the passwords in response to additional info
//	 */
//	private static void analyze() {
//		// TODO analysis
//		
//	}

	/**
	 * add possible passwords to the HashSet
	 */
	public static void getPossibles() {
		input = "";
		while ( !input.equals("done!") ) {
			System.out.print("Word or \"done!\": ");
			input = stdin.next();
			if ( valid(input) && workingLength == 0 ) workingLength = input.length();
			if ( valid(input) && ( input.length() == workingLength )) {
				possibles.add(input.toUpperCase());
			}
		}
		possibles = new ArrayList<>(new LinkedHashSet<>(possibles));
	}
	
	
	/**
	 * validation routine for possible passwords
	 * @param testString a string to validate as a proper possible password
	 * @return true if valid, false if not
	 */
	public static boolean valid(String testString) {
		// use this loop to test character by character
		for ( char element : testString.toCharArray() ) {
			if ( !Character.isLetter(element) ) return false;
		}
		// can insert other criteria for invalid input here
		return true;
	}

}
